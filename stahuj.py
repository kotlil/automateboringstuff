import time
import selenium

import requests
from bs4 import BeautifulSoup

URL = "https://en.wikipedia.org"
START = "/wiki/Special:Random"

def najdi_titulek(soup):
    return soup.find(id="firstHeading").text

def najdi_odkaz(soup):
    pass

def stahuj(stranka):
    while True:
        odpoved = requests.get(URL + stranka)
        odpoved.raise_for_status()

        soup = BeautifulSoup(odpoved.text, "html.parser")
        print(najdi_titulek(soup))

        stranka = najdi_odkaz(soup)
        if not stranka:
            break

        time.sleep(1)


if __name__ == "__main__":
    stahuj(START)
