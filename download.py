import time

import requests
from bs4 import BeautifulSoup


URL = "https://portal.azure.com"
START = ""


def find_head(soup):
    return soup.find(id="firstHeading").text


def find_link(soup):
    pass


def download(page):
    while True:
        answer = requests.get(URL + page)
        answer.raise_for_status()

        soup = BeautifulSoup(answer.text, "html.parser")
        print(find_head(soup))

        page = find_link(soup)
        if not page:
            break

        time.sleep(1)


if __name__ == "__main__":
    download(START)
