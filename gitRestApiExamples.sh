#######################################################
# Gitlab REST API Examples
#######################################################
# API Documentation https://docs.gitlab.com/ee/api/api_resources.html
export GITLAB_SERVER=http://localhost
export GITLAB_TOKEN="./token"
export GITLAB_ROOTGROUP=my-programme
export GITLAB_SUBGROUP=fintech
export GITLAB_PROJECT=hello-world-project-01
export GITLAB_FEATURE=feature/my-feature-01

cat "$token"
#
#creating root group
#
rootGroupId=$(curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_SERVER/api/v4/groups?search=$GITLAB_ROOTGROUP" | jq '.[0]["id"]' )
if [ $rootGroupId == "null" ] 
then
  rootGroupId=$(curl -d "name=$GITLAB_ROOTGROUP&path=$GITLAB_ROOTGROUP&visibility=private&lfs_enabled=true&description=Root group" -X POST "$GITLAB_SERVER/api/v4/groups" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" | jq '.["id"]')
fi
echo "Root group Id: $rootGroupId"
#
#creating sub group
#
rootSubGroupId=$(curl --header "PRIVATE-TOKEN: $GITLAB_TOKEN" "$GITLAB_SERVER/api/v4/groups/$rootGroupId/subgroups?search=$GITLAB_SUBGROUP" | jq '.[0]["id"]' )
if [ $rootSubGroupId == "null" ] 
then
  rootSubGroupId=$(curl -d "name=$GITLAB_SUBGROUP&path=$GITLAB_SUBGROUP&visibility=private&lfs_enabled=true&description=$GITLAB_SUBGROUP programme&parent_id=$rootGroupId" -X POST "$GITLAB_SERVER/api/v4/groups" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" | jq '.["id"]')
fi
echo "Sub group Id: $rootSubGroupId"
#
#project creation
#
projectId=$(curl "$GITLAB_SERVER/api/v4/groups/$rootSubGroupId/projects?search=$GITLAB_PROJECT" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" | jq '.[0]["id"]' )
if [ $projectId == "null" ] 
then
  projectId=projectId=$(curl -d "path=$GITLAB_PROJECT&namespace_id=$rootSubGroupId" -X POST "$GITLAB_SERVER/api/v4/projects" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" | jq '.["id"]')
fi
echo "Project Id: $projectId"
#
#Clonning git project and generating basic java maven structure
# 
mvn archetype:generate -B -DarchetypeGroupId=org.apache.maven.archetypes -DarchetypeArtifactId=maven-archetype-quickstart -DarchetypeVersion=1.4 -DgroupId=edu.emmerson.gitlab -DartifactId=$GITLAB_PROJECT -Dversion=0.0.1
cd $GITLAB_PROJECT
git init
git remote add origin git@localhost:$GITLAB_ROOTGROUP/$GITLAB_SUBGROUP/$GITLAB_PROJECT.git
git add .
git commit -m "Initial commit"
git push -u origin master
#
# Git branches initial structure as per gitflow
#
projectId=$(curl "$GITLAB_SERVER/api/v4/groups/$rootSubGroupId/projects?search=$GITLAB_PROJECT" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" | jq '.[0]["id"]' )
#create develop branch
curl -d "branch=develop&ref=master" -X POST "$GITLAB_SERVER/api/v4/projects/$projectId/repository/branches" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" | jq
#create feature branch
curl -d "branch=$GITLAB_FEATURE&ref=develop" -X POST "$GITLAB_SERVER/api/v4/projects/$projectId/repository/branches" -H "PRIVATE-TOKEN: $GITLAB_TOKEN" | jq
